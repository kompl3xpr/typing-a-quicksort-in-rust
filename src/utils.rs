#[macro_export]
macro_rules! f {
    ($f: path: $arg: ty) => {
        <$arg as $f>::Ret
    };
    ($f: path: $arg: ty, $($args: ty),+) => {
        <($arg, $($args),+) as $f>::Ret
    };
}

pub trait AssertEq { type Ret; }
    impl<T> AssertEq for (T, T) { type Ret = (); }

#[macro_export]
macro_rules! ty_assert_eq {
    ($t1: ty, $t2: ty) => {
        #[allow(unused)]
        const _: f!($crate::utils::AssertEq: $t1, $t2) = ();
    };
}

#[macro_export]
macro_rules! list {
    () => { $carte::list::Nil };
    ($x: ty) => { $crate::list::Cons<$x, $crate::list::Nil> };
    ($x: ty, $($xs: ty),+) => { $crate::list::Cons<$x, list!($($xs),+)> };
}
