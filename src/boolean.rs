pub trait Boolean {}
    pub struct True;
        impl Boolean for True {}
    pub struct False;
        impl Boolean for False {}

pub trait Not: Boolean { type Ret; }
    impl Not for True { type Ret = False; }
    impl Not for False { type Ret = True; }
