mod functional;
mod num;
mod boolean;
mod list;
mod utils;

use functional::*;
use num::*;
use list::*;

trait Quicksort: List { type Ret: List; }
    impl Quicksort for Nil {type Ret = Nil; }
    impl<X: Num, Xs: List> Quicksort for Cons<X, Xs>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  where (LessThan<X>, Xs): Filter, f!(Filter: LessThan<X>, Xs): Quicksort, (NotLessThan<X>, Xs): Filter, f!(Filter: NotLessThan<X>, Xs): Quicksort, ( f!(Quicksort: f!(Filter: LessThan<X>, Xs)), Cons<X, f!(Quicksort: f!(Filter: NotLessThan<X>, Xs))>): Concat,
        { type Ret = f!(Concat: 
            f!(Quicksort: f!(Filter: LessThan<X>, Xs)),
            Cons<X, f!(Quicksort: f!(Filter: NotLessThan<X>, Xs))>); }


type ExpectedList = list![N0, N1, N2, N3, N4, N5, N6, N7, N8, N9];
type ShuffledList = list![N7, N2, N4, N5, N9, N6, N0, N8, N1, N3];    
ty_assert_eq!(ExpectedList, f!(Quicksort: ShuffledList));

fn main() {}