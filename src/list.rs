use std::marker::PhantomData;
use crate::f;

pub trait List {}
    pub struct Nil;
        impl List for Nil {}
    pub struct Cons<X, Xs: List>(PhantomData<(X, Xs)>);
        impl<X, Xs: List> List for Cons<X, Xs> {}

pub trait Concat { type Ret: List; }
    impl<L: List> Concat for (Nil, L) { type Ret = L; }
    impl<X, L1: List, L2: List> Concat for (Cons<X, L1>, L2)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       where (L1, L2): Concat
        { type Ret = Cons<X, f!(Concat: L1, L2)>; }
