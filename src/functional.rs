use crate::boolean::*;
use crate::{list::*, f};

pub trait Apply<A> { type Ret; }

pub trait PushIf { type Ret: List; }
    impl<X, L: List> PushIf for (True, X, L) { type Ret = Cons<X, L>; }
    impl<X, L: List> PushIf for (False, X, L) { type Ret = L; }

pub trait Filter { type Ret: List; }
    impl<F> Filter for (F, Nil) { type Ret = Nil; }
    impl<F: Apply<X>, X, Xs: List, O> Filter for (F, Cons<X, Xs>)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               where (F, Xs): Filter<Ret = O>, (f!(Apply<X>: F), X, O): PushIf,
        { type Ret = f!(PushIf: f!(Apply<X>: F), X, f!(Filter: F, Xs)); }