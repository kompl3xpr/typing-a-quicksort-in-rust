use std::marker::PhantomData;
use crate::{boolean::*, functional::Apply, f};

pub trait Num {}
    pub struct Z;
        impl Num for Z {}
    pub struct S<N: Num>(PhantomData<N>);
        impl<N: Num> Num for S<N> {}

pub type N0 = Z;
pub type N1 = S<N0>; pub type N2 = S<N1>; pub type N3 = S<N2>;
pub type N4 = S<N3>; pub type N5 = S<N4>; pub type N6 = S<N5>;
pub type N7 = S<N6>; pub type N8 = S<N7>; pub type N9 = S<N8>;

pub struct LessThan<N: Num>(PhantomData<N>);
    impl Apply<N0> for LessThan<N0> {type Ret = False; }
    impl<N: Num> Apply<N0> for LessThan<S<N>> { type Ret = True; }
    impl<N: Num> Apply<S<N>> for LessThan<N0> { type Ret = False; }
    impl<N: Num, M: Num> Apply<S<N>> for LessThan<S<M>>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    where LessThan<M>: Apply<N>,
        { type Ret = f!(Apply<N>: LessThan<M>); }

pub struct NotLessThan<N: Num>(PhantomData<N>);
    impl<N: Num, M: Num> Apply<N> for NotLessThan<M>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       where LessThan<M>: Apply<N>, f!(Apply<N>: LessThan<M>): Not
        { type Ret = f!(Not: f!(Apply<N>: LessThan<M>)); }